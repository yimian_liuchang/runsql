package main

import (
	"database/sql"
	"flag"
	"log"
	"time"
	_ "github.com/go-sql-driver/mysql"
)

var (
	argDatabase = flag.String("db", "", "Database")
	argQuery = flag.String("query", "", "Query string")
	argRepeat = flag.Bool("repeat", false, "Repeat until 0 rows affected")
)

func main() {
	flag.Parse()
	if *argDatabase == "" { log.Fatalln("-db arguments required.") }
	if *argQuery == "" { log.Fatalln("-query arguments required.") }

	log.Println("Database: ", *argDatabase)
	log.Println("Query: ", *argQuery)

	db,err := sql.Open("mysql", *argDatabase)
		if err != nil { log.Fatalln(err) }
	for {
		startTime := time.Now()
		result,err := db.Exec(*argQuery)
			if err != nil { log.Fatalln(err) }
		n,err := result.RowsAffected()
			if err != nil { log.Fatalln(err) }
		log.Printf("rows:%d duration(sec):%.2f",
			n, time.Since(startTime).Seconds())
		if !*argRepeat || n == 0 { break }
	}
	db.Close()
}
